
$(document).ready(function() {
   $('.menu-toggler').on('click', function() {
      $(this).toggleClass('open');
      $('.top-nav').toggleClass('open');
   });

   $('.top-nav .nav-link').on('click', function() {
      $('.menu-toggler').removeClass('open');
      $('.top-nav').toggleClass('open');
   });

   $('nav a[href*="#"]').on('click', function() {
      $('html, body').animate({
         scrollTop: $($(this).attr('href')).offset().top -100
   }, 2000);
});

$('#up').on('click', function() {
   $('html, body').animate({
      scrollTop: 0
}, 2000);
});


AOS.init({
   easing:'ease',
   duration: 1800
})
});


// var menuToggler = document.querySelector('.menu-toggler');
// var topNav = document.querySelector('.top-nav');

// function activateNav() {
// menuToggler.classList.toggleClass('open');
// topNav.toggleClass('open');
// }

const checkbox = document.getElementById('dark-light');

checkbox.addEventListener('change', () => {
document.body.classList.toggle('dark');
document.getElementById('offset-heading').classList.toggle('dark');
document.getElementsByClassName('contact').toggleClass('dark');
});